//
//  ViewController.swift
//  0424044
//
//  Created by apple on 2017/3/31.
//  Copyright © 2017年 apple. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.numberOfLines = 0
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBOutlet weak var label: UILabel!
    var s = ""
    @IBAction func b1(_ sender: UIButton) {
        
        var s = ""
       
        for index in 1...9 {
           
            s+="1*\(index)="+"\(1*index)\n"
        }
        label.text = s
    }
    @IBAction func b2(_ sender: UIButton) {
        var s = ""
        
        
        for index in 1...9 {
            
            s+="2*\(index)="+"\(2*index)\n"
        }
        label.text = s
    }

    @IBAction func b3(_ sender: UIButton) {
        var s = ""
        for index in 1...9 {
            
            s+="3*\(index)="+"\(3*index)\n"
        }
        label.text = s
    }
    @IBAction func b4(_ sender: UIButton) {
        var s = ""
        for index in 1...9 {
            
            s+="4*\(index)="+"\(4*index)\n"
        }
        label.text = s
    }
    
    @IBAction func b5(_ sender: UIButton) {
        var s = ""
        for index in 1...9 {
            
            s+="5*\(index)="+"\(5*index)\n"
        }
        label.text = s
    }
    
    @IBAction func b6(_ sender: UIButton) {
        var s = ""
        for index in 1...9 {
            
            s+="6*\(index)="+"\(6*index)\n"
        }
        label.text = s
    }
    @IBAction func b7(_ sender: UIButton) {
        var s = ""
        for index in 1...9 {
            
            s+="7*\(index)="+"\(7*index)\n"
        }
        label.text = s
    }
    @IBAction func b8(_ sender: UIButton) {
        var s = ""
        for index in 1...9 {
            
            s+="8*\(index)="+"\(8*index)\n"
        }
        label.text = s
    }
    @IBAction func b9(_ sender: UIButton) {
        var s = ""
        for index in 1...9 {
            
            s+="9*\(index)="+"\(9*index)\n"
        }
        label.text = s
    }
}

